const express = require('express');

const router = express.Router();

const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

const { verify, verifyAdmin } = auth;

console.log(userControllers);

// REGISTER USER
router.post('/', userControllers.registerUser);
router.post('/checkEmailExists', userControllers.checkEmailExists);

// LOG IN USER / USER AUTHENTICATION
router.post('/login', userControllers.login);

// MAKE ADMIN
router.put('/makeAdmin/:id', verify, userControllers.makeAdmin);

// GET ALL USERS
router.get('/', userControllers.getAllUsers)

// GET SINGLE USER DETAILS
router.get('/getSingleDetails', verify, userControllers.getSingleDetails);

// ADD PRODUCT USER ADMIN ONLY
router.post('/order', verify, userControllers.order);


// ORDER A PRODUCT NON ADMIN
router.post('/order', verify, userControllers.order);

router.get('/getorder', verify, userControllers.getOrder);

// DELETE ORDER
router.patch('/deleteOrder/:id/:order_id', verify, userControllers.deleteOrder)



module.exports = router;
