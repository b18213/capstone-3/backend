// [SECTION] DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const { urlencoded } = require('express');

const app = express();

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.hpwzu.mongodb.net/capstone-3?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedToPology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);

const server = app.listen(process.env.PORT || 5000, () => {
const port = server.address().port;
console.log(`Express is working on port ${port}`);
});
